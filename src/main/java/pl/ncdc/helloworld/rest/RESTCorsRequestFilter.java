package pl.ncdc.helloworld.rest;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: stachu
 * Date: 7/8/16
 * Time: 10:10 PM
 * To change this template use File | Settings | File Templates.
 */
@Provider
@PreMatching
public class RESTCorsRequestFilter implements ContainerRequestFilter {

    private final static Logger log = Logger.getLogger( RESTCorsRequestFilter.class.getName() );

    @Override
    public void filter( ContainerRequestContext requestCtx ) throws IOException {
        log.info( "Executing REST request filter" );
        if ( requestCtx.getRequest().getMethod().equals( "OPTIONS" ) ) {
            log.info( "HTTP Method (OPTIONS) - Detected!" );
            requestCtx.abortWith( Response.status( Response.Status.OK ).build() );
        }
    }
}