/*
 * JBoss, Home of Professional Open Source
 * Copyright 2013, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pl.ncdc.helloworld.rest;




import javax.annotation.PostConstruct;
import javax.ejb.*;
import javax.enterprise.context.SessionScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import pl.ncdc.helloworld.model.*;


/**
 * JAX-RS Example
 * <p/>
 * This class produces a RESTful service to read/write the contents of the members table.
 */
@Local
@Stateless
public class MemberService implements Serializable {

    private final static Logger log = Logger.getLogger(MemberService.class.getName());

    @PersistenceContext(unitName = "chPU")
    private EntityManager entityManager;
    @PostConstruct
    public void postcconstruct() {
        log.info("Calling post construct!" + entityManager);
    }

    public List<Member> listMembers(){
        return entityManager.createNamedQuery("Member.getAll",Member.class).getResultList();//("Member.getAll").getResultList();
    }
    public Member addMember(String name,String lastname,String email,String phone){
        Member n=new Member();

        n.setName(name);
        n.setLastname(lastname);
        n.setEmail(email);
        n.setPhoneNumber(phone);
        entityManager.persist(n);
        return n;

    }

}

