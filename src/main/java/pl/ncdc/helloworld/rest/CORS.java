package pl.ncdc.helloworld.rest;
import javax.ws.rs.NameBinding;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created with IntelliJ IDEA.
 * User: mechu
 * Date: 6/28/16
 * Time: 2:11 PM
 * To change this template use File | Settings | File Templates.
 */
@NameBinding
@Retention(RetentionPolicy.RUNTIME)
public @interface CORS {
    String value() default "*";
}