package pl.ncdc.helloworld.RestService;
import pl.ncdc.helloworld.model.Member;
import pl.ncdc.helloworld.rest.*;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

@Path("/members")
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class RestMemberService{
    @EJB
    private MemberService memberService;

   @GET
   @CORS
   @Path("/list_members")
   public Response listMembers() {
       return Response.ok(new GenericEntity<List<Member>>(memberService.listMembers()) {
       }).build();
   }

   @GET
   @CORS
   @Path("/create_member/{name}/{lastname}/{email}/{phone}")
    public Response createMember(@PathParam("name") String name,
    @PathParam("lastname") String lastname,
    @PathParam("email") String email,
    @PathParam("phone")String phone){

        return Response.ok(memberService.addMember(name,lastname,email,phone)).build();
    }
  /* @POST
   @CORS
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces(MediaType.APPLICATION_JSON)
   public class Response creatMember(Member member){
       Response.ResponseBuilder builder;
        //builder=Response.;
        return builder.build();
    }*/
}